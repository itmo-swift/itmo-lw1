//
//  main.swift
//  lw1
//
//  Created by Daniil Lushnikov on 25.09.2021.
//  Sum of two integers

let a = Int(readLine() ?? "") ?? 0
let b = Int(readLine() ?? "") ?? 0
print(sum(a: a, b: b))
func sum(a: Int, b:Int) -> Int {
    return a + b
}
